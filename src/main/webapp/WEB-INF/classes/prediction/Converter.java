package prediction;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Converter {
	
	// ATTRIBUTS
	private static String data = "";												// response to bemyguideapi getLikes. 											
	
	// Constructeur
	public Converter(){}

	public void setData(String data){Converter.data = data;}
	/*public static void main(String[] args){
		Prediction prediction = new Prediction();
		//prediction.createModel();
		try {
			prediction.predict("./files/arff/bemyguide.arff");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Convert data likes to arff file.
	 *
	 **/
	public void convertDataToARFF(String data, String filename){
		try {
			
			JSONArray rs = new JSONArray(data); 
			FileWriter fwtrain = new FileWriter(filename);
			fwtrain.append("@relation bemyguidetrain\n"
					+ "@attribute weather {cloudy,rain,snow,storm,sunny}\n"
					+ "@attribute pointofinterest {restaurant,zoo|aquarium,movie_theater,art_gallery,cafe,museum,bar,campground,night_club,lodging,park}\n"
					+ "@attribute temperature numeric\n"
					+ "@attribute distance numeric\n"
					+ "@attribute month {0,1,2,3,4,5,6,7,8,9,10,11}\n"
					+ "@attribute day {0,1,2,3,4,5,6}\n"
					+ "@attribute times numeric\n"
					+ "@attribute rating numeric\n"
					+ "@attribute liked {0,1}\n\n"
					+ "@data\n");
					
			for(int i=0 ; i< rs.length(); i++){
				JSONObject elmt = rs.getJSONObject(i);
				fwtrain.append((elmt.has("nameWeather")?elmt.getString("nameWeather"):"?"));
				fwtrain.append(',');
				fwtrain.append(elmt.getString("nameGoogle"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("temperature")?elmt.getDouble("temperature"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("distance")?elmt.getInt("distance"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("month")?elmt.getInt("month"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("day")?elmt.getInt("day"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("times")?elmt.getInt("times"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("rating")?elmt.getDouble("rating"):"?"));
				fwtrain.append(',');
				fwtrain.append(""+(elmt.has("liked")?elmt.getInt("liked"):"?"));
				fwtrain.append('\n');
			}
			fwtrain.flush();
			fwtrain.close();
			
			System.out.println("ARFF Files are created successfully");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Convert data likes to csv file.
	 *
	 **/
	public void convertDataToCSV(String data, String filename){
		try {
			
			JSONArray rs = new JSONArray(data); 
			int nbtrain = 10*rs.length()/10;
			
			
			FileWriter fwtrain = new FileWriter(filename);
			fwtrain.append("weather,pointofinterest,temperature,distance,month,day,times,rating,liked\n");
			
					
			for(int i=0 ; i< rs.length(); i++){
				JSONObject elmt = rs.getJSONObject(i);
				fwtrain.append(elmt.getString("nameWeather"));
				fwtrain.append(',');
				fwtrain.append(elmt.getString("nameGoogle"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getDouble("temperature"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getInt("distance"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getInt("month"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getInt("day"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getInt("times"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getDouble("rating"));
				fwtrain.append(',');
				fwtrain.append(""+elmt.getInt("liked"));
				fwtrain.append('\n');
			}
			fwtrain.flush();
			fwtrain.close();
			System.out.println("CSV Files are created successfully");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
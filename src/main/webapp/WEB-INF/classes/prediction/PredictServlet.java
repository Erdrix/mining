package prediction;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class PredictServlet extends HttpServlet{
	
	String path = System.getenv("OPENSHIFT_DATA_DIR");
	String filenameToPredict = path+"/files/arff/bemyguide.arff";
	// On créer un modèle.
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		PrintWriter pw = response.getWriter();

		
		Converter  converter = new Converter();
		Prediction prediction = new Prediction();
		JSONObject jsonObj = new JSONObject();
		String result ="";
		ArrayList<String> listPlaces= new ArrayList<>();
		ArrayList<Integer> likesPrediction = new ArrayList<>();
		JSONArray mPlaces = new JSONArray();
		
		try {
			BufferedReader buffer = request.getReader();
            String s = "";
            while ((s = buffer.readLine()) != null) {
            	result += s;
            }
            System.out.println("test : "+result);
			// On converti la requête en .arff
			JSONObject requestJson = new JSONObject(result);
			String splaces = requestJson.getJSONArray("places").toString(); 
			//"[{nameWeather:sunny,nameGoogle:restaurant,temperature:10,distance:500,month:7,day:5,times:12,rating:4.5, id:'5555'}]";
			converter.convertDataToARFF(splaces, filenameToPredict);

			// On prédit les éléments qui vont intéresser l'utilisateur.
			
			likesPrediction = prediction.predict(filenameToPredict);
			// On récupère la liste
			mPlaces = new JSONArray(splaces);
			for(Integer i : likesPrediction){
				JSONObject place = mPlaces.getJSONObject(i);
				listPlaces.add(place.getString("id"));
			}
			JSONArray places = new JSONArray(listPlaces);
			jsonObj.put("placesLiked", places);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pw.println(jsonObj.toString());
	}
}

package prediction;

import java.io.File;
import java.util.Enumeration;

import java.util.ArrayList;
import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Prediction {

	private static String path = System.getenv("OPENSHIFT_DATA_DIR");
	private static String filenameTrainCSV =path+"/files/csv/bemyguidetrain.csv";		// path to store train csv file.
	private static String filenameTrainARFF =path+"/files/arff/bemyguidetrain.arff";	// path to store train csv file.
	
	public void createModel(){
		Converter converter = new Converter();
		LikesAsyncTask cvTask  =new LikesAsyncTask();
		try {

			// Extract data training
			converter.convertDataToARFF(cvTask.sendGet(), filenameTrainARFF);
			converter.convertDataToCSV(cvTask.sendGet(), filenameTrainCSV);		
		
			// Create classifieur
			DataSource sourceTrain;
			Instances instancesTrain = null;
			
			// donnees pour l'apprentissage (train)
			sourceTrain = new DataSource(filenameTrainARFF);
			instancesTrain = sourceTrain.getDataSet();
			instancesTrain.setClassIndex(instancesTrain.numAttributes()- 1);
	
			// instancier le classifieur
			J48 classifieur = new J48();
						
			System.out.println("System --- Construction du classifieur J48");
			
			// construction du classifieur avec les donnees d'entraînement
			classifieur.buildClassifier(instancesTrain);
			
			// On serialize maintenat le classifieur
			weka.core.SerializationHelper.write(path+"/files/classifieur/j48.model", classifieur);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	public ArrayList<Integer> predict(String filenamePlaces) throws Exception{
		
		ArrayList<Integer> likePrediction = new ArrayList<>();
		// on récupère le classifieur
		Classifier classifieur = (Classifier) weka.core.SerializationHelper.read(path+"/files/classifieur/j48.model");
					
		// System -- Classement des fichiers
		DataSource source = new DataSource(filenamePlaces);
		Instances dataset = source.getDataSet();						
		dataset.setClassIndex(dataset.numAttributes()- 1);
		System.out.println("Système A --- Classement des Places :");
		Enumeration e = dataset.enumerateInstances();
		int j = 0;
		
		// Affichage du résultat.
		String result="";
		while(e.hasMoreElements()){
			Instance i = (Instance)e.nextElement();
			double prediction = classifieur.classifyInstance(i);		
			result += "Place ["+j+"] = ";
			if(dataset.classAttribute().value((int)prediction).equals("0")){
				result+="J'aime pas\n";
			}else{
				result+="J'aime\n";
				likePrediction.add(j);

			}
			j++; 
		}
		//return result;
		return likePrediction;
	}
}

package prediction;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class CreateModelServlet extends HttpServlet{

	// On créer un modèle.
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("/predict/CreateModel");
		PrintWriter pw = response.getWriter();
		Prediction prediction = new Prediction();
		prediction.createModel();
        pw.println("Ok");
	}
}